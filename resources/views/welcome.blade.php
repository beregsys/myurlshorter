@extends('layout')
@section('content')
    <div class="container">

        <div class="row">
            <div class="col-12">
                <h1>Let's go!</h1>
            </div>

            <div class="col-sm-12 col-md-6">
                <form id="myForm" method="POST" action="/create">
                    <div class="form-group">
                        <label for="baseUrl">Enter URL</label>
                        <input type="text" class="form-control" id="baseUrl" name="base_url" placeholder="Enter base url" >
                    </div>
{{--                    <div class="form-group">--}}
{{--                        <label for="backHalf">Enter back-half</label>--}}
{{--                        <input type="text" class="form-control" id="backHalf" name="back_half" placeholder="Enter back-half for url" >--}}
{{--                    </div>--}}
                    @csrf
                    @method('PUT')
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection