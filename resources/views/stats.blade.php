@extends('layout')
@section('content')
    <div class="container">
        <h1>Statistics</h1>
        <div class="row">
            <a href="/">Home</a>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Date of each use</th>
                    <th scope="col">IP</th>
                    <th scope="col">City</th>
                    <th scope="col">Browser</th>
                    <th scope="col">OS</th>
                </tr>
                </thead>
                <tbody>
                    @if(count($stats) > 0)
                        @foreach($stats as $stat)
                            <tr>
                                <th>{{ $stat->created_at }}</th>
                                <td>{{ $stat->ip }}</td>
                                <td>{{ $stat->locations }}</td>
                                <td>{{ $stat->browser }}</td>
                                <td>{{ $stat->os }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <th>There are no statistics for this url</th>
                        </tr>

                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection



