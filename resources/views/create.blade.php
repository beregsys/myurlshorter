@extends('layout')
@section('content')
    <div class="container">
        <h1>You links is added!</h1>
        <div class="row">
            <div class="col-12">
                <p>{{ Request::root() . '/' . $body['back_half']}}</p>
            </div>
            <div class="col-12">
                <p>{{ Request::root() . '/stat/' . $body['back_half']}}</p>
            </div>
        </div>
        <div class="row">
            <a href="/">Home</a>
        </div>
    </div>
@endsection