<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statistics extends Model
{
	protected $fillable = ['id_links', 'ip', 'locations', 'browser', 'os'];
}
