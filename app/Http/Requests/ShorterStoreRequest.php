<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShorterStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
	        'base_url' => 'required'
        ];
    }
}
