<?php

namespace App\Http\Controllers;
use App\Http\Requests\ShorterStoreRequest;
use http\Client;
use Illuminate\Support\Facades\Redirect;
use App\Link;
use App\Statistics;
use Carbon\Carbon;
use GuzzleHttp;
use Jenssegers\Agent\Agent;

use Illuminate\Http\Request;


class ShorterController extends Controller
{

	public function index($shorter) {
			$stat_link = Link::where( 'back_half', '=', $shorter )->firstOrFail();
			$stats = Statistics::whereIdLinks($stat_link->id)->get();
			return view('stats', compact('stats'));
	}

    public function show($shorter, Request $request) {
		$links = Link::where( 'back_half', '=', $shorter )->firstOrFail();
		$current_date_time = Carbon::now();
		$current_date_time = $current_date_time->toDateTimeString();
		$current_ip = $request->getClientIp(true);
	    $client = new GuzzleHttp\Client();
	    $response = $client->request('GET', 'https://ipinfo.io/' . $current_ip .'/geo/?token=a528e29a87426a');
		$body = json_decode($response->getBody());
	    $city = (isset($body->city)) ?: 'You on localhost, can\'t detect location';
	    $agent = new Agent();
	    $platform = $agent->platform();
	    $browser = $agent->browser();
	    $statistics = new Statistics([
			'id_links'  =>      $links->id,
		    'use_date'  =>      $current_date_time,
		    'ip'        =>      $current_ip,
		    'locations' =>      $city,
		    'browser'   =>      $browser,
		    'os'        =>      $platform
	    ]);
	    $statistics->save();
		return Redirect::to($links->base_url);
	}

	public function store(ShorterStoreRequest $request) {
		$data = $request->all();
		$data['back_half'] = $this->get_back_half(5);
		$link = Link::create($data);
		return view('create', ['body' => $link->toArray()]);
	}

	public function get_back_half($quantity) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_+';
		$randomString = '';

		for ($i = 0; $i < $quantity; $i++) {
			$index = rand(0, strlen($characters) - 1);
			$randomString .= $characters[$index];
		}

		return $randomString;
	}

}
